import { Component, OnInit } from '@angular/core';
import {SearchService} from '../core/services/search.service';
import {NewsInterface} from '../core/interface/news.interface';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {

    newsItems: NewsInterface[];

constructor(private searchService : SearchService) { }

ngOnInit() {
    this. searchService.search('typeScript')
        .subscribe({
            next: data => {
                this.newsItems= data['items'];
            }
        })
}

}
