import { Component, OnInit } from '@angular/core';
import {NewsInterface} from '../../core/interface/news.interface';
import {SearchService} from '../../core/services/search.service';


@Component({
  selector: 'app-news-list',
  templateUrl: './news-list.component.html',
  styleUrls: ['./news-list.component.css']
})
export class NewsListComponent implements OnInit {

    newsItems: NewsInterface[];

    constructor(private searchService : SearchService) { }

    ngOnInit() {
        this. searchService.search('angular')
            .subscribe({
                next: data => {
                    this.newsItems= data['items'];
                }
            })
    }

}
