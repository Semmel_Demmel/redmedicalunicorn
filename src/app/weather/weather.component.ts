import { Component, OnInit } from '@angular/core';

import weatherdata from '../../assets/data/weatherdata.json';
import {SearchService} from '../core/services/search.service';
import {NewsInterface} from '../core/interface/news.interface';


@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {

    weather: any[];
    newsItems: NewsInterface[];


  constructor(private searchService: SearchService) { }

  ngOnInit() {
      this.weather = weatherdata;
      this. searchService.search('weather')
          .subscribe({
              next: data => {
                  this.newsItems = data['items'];
              }
          })
  }
}
