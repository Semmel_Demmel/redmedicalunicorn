import {NgModule} from "@angular/core";
import {NavbarComponent} from "./navbar/navbar.component";
import {MenuComponent} from "./menu/menu.component";
import {AppRoutingModule} from "../../app.routing.module";
import {BrowserModule} from "@angular/platform-browser";
import { HeaderComponent } from './header/header.component';

@NgModule({
  imports: [
    AppRoutingModule,
    BrowserModule
  ],
  declarations: [
    NavbarComponent,
    MenuComponent,
    HeaderComponent
  ],
  exports: [
    NavbarComponent,
    MenuComponent
  ]
})
export class LayoutModule {

}
